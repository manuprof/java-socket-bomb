/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverbomb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author STUDENTE
 */
public class Serverbomb {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(9999);
        // in attesa del client
        System.out.print("In attesa del client...");
        Socket socketClient = socket.accept();
        boolean finito = false;
        System.out.println("Connesso!");
        // da qui avviene lo scambio della bomba
        // generiamo la bomba
        int bomb = ((int) (Math.random() * 10) + 1);
        System.out.println("Bomba generata: " + bomb);

        do {

            PrintWriter out = new PrintWriter(socketClient.getOutputStream(), true);
            // scrivo il messaggio
            System.out.println("Spedisco la bomba: " + bomb);
            out.println(bomb);

            if (bomb > 1) {
                // in attesa di ricevere la bomba indietro
                BufferedReader in = null;
                in = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
                // leggo il messaggio e sovrascrivo la bomba con il nuovo valore
                bomb = Integer.parseInt(in.readLine());

                System.out.println("Valore della bomba sul server: " + bomb);
                // controlliamo se la bomba è esplosa
                bomb--;
                if (bomb <= 0) {
                    System.out.println("Bomba esplosa sul server!!!");
                    finito = true;
                }

            } else {
                finito = true;
            }

        } while (!finito);

        System.out.println("Gioco finito sul server.");

    }

}
