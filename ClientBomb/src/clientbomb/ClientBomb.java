/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientbomb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author STUDENTE
 */
public class ClientBomb {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 9999);
            BufferedReader in = null;
            boolean finito = false;
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            int bomb = 0;
            do {
                // leggo la bomba
                bomb = Integer.parseInt(in.readLine());
                System.out.println("Valore della bomba sul client: " + bomb);

                bomb--;
                // controlliamo se la bomba è esplosa
                if (bomb <= 0) {
                    // bomba esplosa
                    System.out.println("Bomba esplosa sul client!!!");
                } else {
                    System.out.println("Spedisco la bomba al server: "+bomb);
                    // decremento la bomba (diminuire la miccia)
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    // rispedisco la bomba indietro
                    out.println(bomb);
                }
                // gioco finito quando la bomba è esplosa o manca 1 all'esplosione
                if (bomb <= 1) {
                    finito = true;
                }

            } while (!finito);

            System.out.println("Gioco finito sul client.");

        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
    }

}
